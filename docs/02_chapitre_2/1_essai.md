---
author: JL Harel
title: Un premier essai Python
tags:
  - fonction
---

La fonction `premiere` prend en paramètre un nombre **quelconque** et renvoie le double de ce nombre

!!! example "Exemple"

    ```pycon
    >>> premiere(3)
    6
    ```



???+ question "Compléter le code ci-dessous"

    {{ IDE('scripts/premiere') }}
