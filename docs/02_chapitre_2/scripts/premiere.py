# --------- PYODIDE:code --------- #

def premiere(mon_nombre):
    ...


# --------- PYODIDE:corr --------- #

def premiere(mon_nombre):
    return mon_nombre*2


# --------- PYODIDE:tests --------- #

assert premiere(9) == 18

# --------- PYODIDE:secrets --------- #

assert premiere(2)== 4

