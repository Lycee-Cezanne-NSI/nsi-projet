---
author: JL Harel
title: Une fonction simple
tags:
  - fonction
---

Compléter la fonction `addition` qui prend en paramètre deux nombres entiers ou flottants, et renvoie la somme des deux.

!!! example "Exemple"

    ```pycon
    >>> addition(2, 3)
    5
    ```

???+ question "Compléter le code ci-dessous"

    {{ IDE('scripts/addition') }}
